import 'package:flutter/material.dart';
import 'package:pety/UI/Appbar.dart';
import 'package:pety/Models/Event.dart';
import 'package:pety/Repository.dart';

class EventForm extends StatefulWidget {
  EventForm(this.repo);

  final Repository repo;
  @override
  _EventFormState createState() => _EventFormState();
}

class _EventFormState extends State<EventForm> {
  final _formKey = GlobalKey<FormState>();
  String title;
  String subtitle;
  String description;
  String location;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: RoundedAppBar(title: 'Dodaj Event'),
      body: Center(
        child: Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  TextFormField(
                    onSaved: (value) {
                      setState(() {
                        title = value;
                      });
                    },
                    decoration: InputDecoration(
                      labelText: 'Tytuł',
                      fillColor: Colors.green,
                      focusColor: Colors.yellow,
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Dodaj tytuł';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    onSaved: (value) {
                      setState(() {
                        subtitle = value;
                      });
                    },
                    decoration: InputDecoration(
                      labelText: 'Podtytuł',
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Dodaj podtytuł';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    onSaved: (value) {
                      setState(() {
                        description = value;
                      });
                    },
                    decoration: InputDecoration(
                      labelText: 'Opis',
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Dodaj opis';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    onSaved: (value) {
                      setState(() {
                        location = value;
                      });
                    },
                    decoration: InputDecoration(
                      labelText: 'Lokalizacja',
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Dodaj lokacje';
                      }
                      return null;
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Center(
                      child: RaisedButton(
                        padding: EdgeInsets.all(0),
                        color: Colors.green,
                        onPressed: () {
                          // Validate returns true if the form is valid, or false
                          // otherwise.
                          if (_formKey.currentState.validate()) {
                            final form = _formKey.currentState;
                            // If the form is valid, display a Snackbar.
                            form.save();
                            Event event = Event(
                                title: title,
                                subtitle: subtitle,
                                description: description,
                                location: location);
                            widget.repo.addEvent(event);
                            Navigator.pop(context);
                          }
                        },
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                        child: Text(
                          'Wyślij',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                          ),
                        ),
                      ),
                    ),
                  ),

                ],
              ),
            )),
      ),
    );
  }
}
