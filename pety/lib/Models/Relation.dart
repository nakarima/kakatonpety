class Relation {
  final String imageUrl;
  final String videoUrl;
  final String title;
  final String description;
  final String location;
  int points = 0;
  bool voted = false;
  bool addPoint = false;
  bool substractPoint = false;

  Relation({this.title, this.description, this.imageUrl, this.location, this.videoUrl});

  factory Relation.fromJson(Map<String, dynamic> json) {
    return Relation(
      title: json['title'],
      description: json['description'],
      location: json['localization'],
      videoUrl: json['videoUrl'],
      imageUrl: json['imageUrl'],

    );

  }
}