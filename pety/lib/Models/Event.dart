class Event {
  final String title;
  final String subtitle;
  final String description;
  final String location;
  bool saved = false;
  int id;

  Event({this.title, this.description, this.subtitle, this.location, this.id});

  factory Event.fromJson(Map<String, dynamic> json) {
    return Event(
        title: json['title'],
        subtitle: json['subtitle'],
        id: json['id'],
        description: json['description'],
        location: json['localization'],

    );

  }
}