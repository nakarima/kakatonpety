import 'package:flutter/material.dart';
import 'package:pety/UI/Appbar.dart';
import 'package:pety/Models/Relation.dart';
import 'package:pety/Repository.dart';

class RelationForm extends StatefulWidget {
  RelationForm(this.repo, this.id);

  final Repository repo;
  final int id;
  @override
  _RelationFormState createState() => _RelationFormState();
}

class _RelationFormState extends State<RelationForm> {
  final _formKey = GlobalKey<FormState>();
  String title;
  String description;
  String imageUrl;
  String videoUrl;
  String location;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: RoundedAppBar(title: 'Dodaj relacje'),
      body: ListView(
        children: <Widget>[
          Center(
            child: Form(
                key: _formKey,
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[

                      TextFormField(
                        onSaved: (value) {
                          setState(() {
                            imageUrl = value;
                          });
                        },
                        decoration: InputDecoration(
                          labelText: 'Adres zdjęcia',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Dodaj adres';
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        onSaved: (value) {
                          setState(() {
                            videoUrl = value;
                          });
                        },
                        decoration: InputDecoration(
                          labelText: 'Adres filmu',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Dodaj adres';
                          }
                          return null;
                        },
                      ),
                      Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: 10),
                        child: RaisedButton(
                          color: Colors.green,
                          padding: EdgeInsets.all(10),
                          onPressed: () {
                            // Validate returns true if the form is valid, or false
                            // otherwise.
                            if (_formKey.currentState.validate()) {
                              final form = _formKey.currentState;
                              // If the form is valid, display a Snackbar.
                              form.save();
                              Relation relation = Relation(
                                  imageUrl: imageUrl,
                                  videoUrl: videoUrl);
                              widget.repo.addRelation(relation, widget.id);
                              Navigator.pop(context);
                            }
                          },
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                          ),
                          child: Center(

                            child: Text(
                              'Wyślij',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 15.0,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )),
          ),
        ],
      ),
    );
  }
}
