import 'package:flutter/material.dart';
import 'package:pety/UI/Appbar.dart';
import 'package:pety/Models/Relation.dart';
import 'package:video_player/video_player.dart';
import 'package:chewie/chewie.dart';

class RelationPage extends StatefulWidget {
  RelationPage(this.relation);

  final Relation relation;

  @override
  _RelationPageState createState() => _RelationPageState();
}

class _RelationPageState extends State<RelationPage> {
  VideoPlayerController _videoPlayerController;
  ChewieController _chewieController;

  @override
  void initState() {
    super.initState();
    _videoPlayerController = VideoPlayerController.network(
      widget.relation.videoUrl);
    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController,
      aspectRatio: 3 / 2,
      autoPlay: true,
      looping: true,
      // Try playing around with some of these other options:

      // showControls: false,
      // materialProgressColors: ChewieProgressColors(
      //   playedColor: Colors.red,
      //   handleColor: Colors.blue,
      //   backgroundColor: Colors.grey,
      //   bufferedColor: Colors.lightGreen,
      // ),
      // placeholder: Container(
      //   color: Colors.grey,
      // ),
      // autoInitialize: true,
    );
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    _chewieController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: RoundedAppBar(
        title: widget.relation.title,
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(15),
          ),
          Center(
            child: Chewie(
              controller: _chewieController,
            ),
          ),
          /*Padding(
            padding: EdgeInsets.all(15),
          ),
          Image(
            image: AssetImage(widget.relation.image),
            height: 300,
          ),*/
          Padding(
            padding: EdgeInsets.all(15),
            child: Text(
              'Co zrobiłem?',
              style: TextStyle(fontSize: 25),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(15),
            child: Text(
              widget.relation.description,
              style: TextStyle(fontSize: 20),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(15),
            child: Text(
              'Gdzie?',
              style: TextStyle(fontSize: 25),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(15),
            child: Text(
              widget.relation.location,
              style: TextStyle(fontSize: 20),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(15),
            child: Text(
              'Punkty:',
              style: TextStyle(fontSize: 25),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(15),
            child: Text(
              widget.relation.points.toString(),
              style: TextStyle(fontSize: 20),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              RaisedButton(
                padding: EdgeInsets.all(0),
                onPressed: (widget.relation.substractPoint)
                    ? null
                    : () {
                        setState(() {
                          widget.relation.substractPoint = true;
                          widget.relation.addPoint = false;
                          if (widget.relation.voted)
                          {
                            widget.relation.points-=2;
                          } else {
                            widget.relation.voted = true;
                            widget.relation.points--;
                          }
                        });
                      },
                color: Colors.red,
                disabledColor: Colors.red,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
                child: Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 40.0, vertical: 10.0),
                  child: Text(
                    '-1',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 15.0,
                    ),
                  ),
                ),
              ),
              RaisedButton(
                padding: EdgeInsets.all(0),
                onPressed: (widget.relation.addPoint)
                    ? null
                    : () {
                        setState(() {
                          widget.relation.addPoint = true;
                          widget.relation.substractPoint = false;
                          if (widget.relation.voted)
                            {
                              widget.relation.points+=2;
                            } else {
                            widget.relation.voted = true;
                            widget.relation.points++;
                          }
                        });
                      },
                disabledColor: Colors.green,
                color: Colors.green,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
                child: Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 40.0, vertical: 10.0),
                  child: Text(
                    '+1',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 15.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.all(20),
          )
        ],
      ),
    );
  }
}
