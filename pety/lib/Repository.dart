import 'package:pety/Models/Event.dart';
import 'package:pety/Models/Relation.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

import 'dart:async';
import 'dart:convert';

class Repository {

  List<Event> savedEvents = [];


  Future<List<Event>> fetchEvents() async {
    final response = await http.get('http://pazie.pl:8080/events');

    if (response.statusCode == 200) {
      List<Event> t;
      var data = json.decode(response.body);
      var list = data['events'] as List;
      t = list.map((json) => Event.fromJson(json)).toList();

      return t;
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load points');
    }
  }

  Future<List<Relation>> fetchRelations() async {
    final response = await http.get('http://pazie.pl:8080/stories');

    if (response.statusCode == 200) {
      List<Relation> t;
      var data = json.decode(response.body);
      var list = data['stories'] as List;
      t = list.map((json) => Relation.fromJson(json)).toList();

      return t;
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load points');
    }
  }

  getSavedEvents() {
    return savedEvents;
  }



  addEvent(Event event) async {
    // set up POST request arguments
    String url = 'http://pazie.pl:8080/events';
    Map<String, String> headers = {"Content-type": "application/json"};
    String json = '{"maintainerId": 2137, "title":"${event.title}", "subtitle":"${event.subtitle}", "description": "${event.description}", "localization": "${event.location}"}';
    // make POST request
    Response response = await post(url, headers: headers, body: json);
    // check the status code for the result
    int statusCode = response.statusCode;
    // this API passes back the id of the new item added to the body
    String body = response.body;
    // {
    //   "title": "Hello",
    //   "body": "body text",
    //   "userId": 1,
    //   "id": 101
    // }
  }

  addRelation(Relation relation, int id) async {
    // set up POST request arguments
    String url = 'http://pazie.pl:8080/events/${id}/sendProves';
    Map<String, String> headers = {"Content-type": "application/json"};
    String json = '{"videoUrl":"${relation.videoUrl}", "imageUrl":"${relation.imageUrl}"}';
    // make POST request
    Response response = await post(url, headers: headers, body: json);
    // check the status code for the result
    int statusCode = response.statusCode;
    // this API passes back the id of the new item added to the body
    String body = response.body;
    // {
    //   "title": "Hello",
    //   "body": "body text",
    //   "userId": 1,
    //   "id": 101
    // }
  }



  saveEvent(Event event) {
    if (event.saved) {
      savedEvents.remove(event);
      event.saved = false;
    } else {
      savedEvents.add(event);
      event.saved = true;
    }

  }
}
